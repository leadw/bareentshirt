
var $jq = jQuery.noConflict();

$jq(document).ready(function() {
  SliderInit();
  deliveryEstimate();
})

const SliderInit = function () {
  $jq('[data-hor-slider]').slick({
    dots: true,
    arrows: true,
    prevArrow: '<button type="button" class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" fill="none"><path d="M15 6L9 12L15 18" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/></svg></button>',
    nextArrow: '<button type="button" class="slick-next"><svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" fill="none"><path d="M9 6L15 12L9 18" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/></svg></button>'
  });
}

const deliveryEstimate = function() {

  if($jq('.estimate_delivery_date').length > 0) {
    insertDeliveryLabel();
    $jq(document).on('click', '.btnCartToggle', function() {
      setTimeout(function() {
        console.log("here");
        insertDeliveryLabel();
      }, 1000);
    })
  }
  
}

function insertDeliveryLabel() {
  Date.prototype.addDays = function(days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
  }

  var today = new Date();
  var hr = today.getHours();
  var gDay = today.getDay();
  var d_text = '';

  var idex = 0;

  var $obj = $jq('.estimate_delivery_date');
  var p_text = $obj.attr('data-prefix');
  var time_axis = parseInt($obj.attr('data-time'));
  var ft_weekend = false;
  var time_axis_weekend = $obj.attr('data-time-weekend');
  if (time_axis_weekend != null && time_axis_weekend != undefined && time_axis_weekend != 'undefined') {
    ft_weekend = true;
    time_axis_weekend = parseInt(time_axis_weekend);
  }
  var days_in_time = parseInt($obj.attr('data-days-before-time'));
  var days_after_time = parseInt($obj.attr('data-days-after-time'));


  if(ft_weekend && gDay == 6) {
    time_axis = time_axis_weekend;
  }

  if(hr < time_axis) {
    idex = days_in_time;
    if(gDay == 6 || ((gDay + days_in_time) > 6)) {
      idex += 1;
    }
  }
  else {
    idex = days_after_time;
    if(gDay == 6 || ((gDay + days_after_time) > 6)) {
      idex += 1;
    }
  }

  var del_date = today.addDays(idex);
  var d_day = del_date.getDay();
  var d_month = del_date.getMonth();

  var f_date = del_date.toLocaleDateString('en-us', {weekday:"long", month:"short", day:"numeric"});

  var f_d_day = (f_date.split(','))[0];
  var f_d_month = (f_date.split(' '))[1];
  var f_d_date = (f_date.split(' '))[2];

  if($obj.attr('data-trans-days') != null) {
    d_day -= 1;
    if(d_day == -1) {
      d_day = 6;
    }
    f_d_day = (($obj.attr('data-trans-days')).split(','))[d_day];
  }

  if($obj.attr('data-trans-months') != null) {
    f_d_month = (($obj.attr('data-trans-months')).split(','))[d_month];
  }

  d_text = p_text + ' ' + f_d_day + ', ' + f_d_date + '. ' + f_d_month;

  $jq('.estimate_delivery_date').each(function() {
    $jq(this).text(d_text);
  });

}